import { expect } from 'chai'
import PouchDB from 'pouchdb';
import { Gtdb } from "../src/Gtdb";


describe('Gtdb', () => {
	describe('makeGtdb',() => {
		it('should make the test DB', async () => {
			const gtdb = new Gtdb()
			const gtdbFile = './data/bac_taxonomy_r86.test.tsv'
			return gtdb.changeDatabaseName('gtdb-test')
				.changeGtdbFile(gtdbFile)
				.makeDB().then(() => {
				const db = new PouchDB('gtdb-test')
				return db.info().then((info) => {
					expect(info.doc_count).gte(100)
					db.close()
				})
			})
		})
	})
	describe('createIndex', () => {
		it('should create the index', async () => {
			const expected = [false, false, false, false, false, false, false]
			const gtdb = new Gtdb()
			const gtdbFile = './data/bac_taxonomy_r86.test.tsv'
			await gtdb.changeDatabaseName('gtdb-test')
				.changeGtdbFile(gtdbFile)
				.createIndexes()
				.then((msg: any) => {
					const results: boolean[] = []
					msg.forEach((item: string) => {
						results.push(item.match(/created|exists/) === null)
					})
					return expect(results).eql(expected)
				})
		})
	})
	describe('making searches', () => {
		it('should get all records from Campylobacter phylum', () => {
			const gtdb = new Gtdb()
			const gtdbFile = './data/bac_taxonomy_r86.test.tsv'
			return gtdb.changeDatabaseName('gtdb-test')
				.changeGtdbFile(gtdbFile)
				.getDB()
				.then((db) => {
					return db.find({
						selector: {
							p: 'Proteobacteria'
						},
					})
				})
				.then((data: any) => {
					return expect(data.docs.length).eql(40)
				})
		})
		it('should get all records from Campylobacterota phylum that are not Campylobacterales', () => {
			const gtdb = new Gtdb()
			const gtdbFile = './data/bac_taxonomy_r86.test.tsv'
			return gtdb.changeDatabaseName('gtdb-test')
				.changeGtdbFile(gtdbFile)
				.getDB()
				.then((db) => {
					return db.find({
						selector: {
							'$and': [
								{
									p: 'Proteobacteria'
								},
								{
									'$not': {
										c: 'Gammaproteobacteria'
									}
								} 
							]
						}
					})
				})
				.then((data: any) => {
					gtdb.getDB().then((db) => db.close())
					return expect(data.docs.length).eql(3)
				})
		})
	})
})
