interface ITaxonomyInfo {
    _id: string,
    d: string,
    p: string,
    c: string,
    o: string,
    f: string,
    g: string,
    s: string
}

interface IOptions {
    logLevel?: number,
}

export {
    ITaxonomyInfo,
    IOptions
}