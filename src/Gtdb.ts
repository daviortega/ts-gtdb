'use strict'

import bunyan from 'bunyan'
import * as fs from 'fs'
import PouchDB from 'pouchdb'
import pouchdbFind from 'pouchdb-find'
import * as rimraf from 'rimraf'
import { Transform } from 'stream';

PouchDB.plugin(pouchdbFind)

import { IOptions, ITaxonomyInfo } from './GtdbInterfaces';

const logAlias = {
	'debug': 20,
	'error': 50,
	'fatal': 60,
	'info': 30,
	'trace': 10,
	'warn': 40,
}

class Gtdb {
	private db: any
	private options: IOptions
	private log: bunyan
	private buffer: string
	private gtdbFile: string
	private name: string

	constructor(options?: IOptions) {
		this.options = options || {}
		this.name = 'gtdb'
		this.log = bunyan.createLogger({
			level: this.options.logLevel || 30,
			name: 'Gtbd',
		})
		this.buffer = ''
		this.db = ''
		this.gtdbFile = './data/bac_taxonomy_r86.tsv'
	}

	public getDB() {
		return this.loadDB().then(() => this.db)
	}

	public changeGtdbFile(newFile: string): Gtdb {
		this.gtdbFile = newFile
		return this
	}

	public changeDatabaseName(databaseName: string): Gtdb {
		this.name = databaseName
		return this
	}

	public async makeDB(): Promise<null> {
		this.db = new PouchDB(this.name)
		return new Promise((resolve, reject) => {
			const parseLine = (line: string):ITaxonomyInfo => {
				try {
					const fields = line.split('\t')
					const id = fields.shift() || ''
					const tax = fields[0].split(';')
					const d = tax[0].split('__')[1] || '';
					const p = tax[1].split('__')[1] || '';
					const c = tax[2].split('__')[1] || '';
					const o = tax[3].split('__')[1] || '';
					const f = tax[4].split('__')[1] || '';
					const g = tax[5].split('__')[1] || '';
					const s = tax[6].split('__')[1] || '';
					const taxonomy:ITaxonomyInfo = { _id: id, c, d, f, g, o, p, s}        
					return taxonomy
				}
				catch (e) {
					this.log.fatal(`Line was not parsed successfuly\nline: ${line}\n`)
					throw e
				}
			}
	
			let buffer = ''
			const pass = new Transform({
				transform: (async (chunk, enc, next) => {
					const dataRaw = buffer + chunk.toString()
					const data = dataRaw.split('\n')
					buffer = data.pop() || ''
					const entries:ITaxonomyInfo[] = []
					for (const line of data) {
						entries.push(parseLine(line))
					}
					await this.db.bulkDocs(entries).catch((err: any) => {
						throw err
					})
					this.log.debug(`Added ${data.length} entries to the DB`)
					next()
				})
			})
			pass.on('finish', async () => {
				this.log.info(`All set building ${this.name} database`)
				this.db.close().then(resolve)
			})
			const readDatabaseFile = fs.createReadStream(this.gtdbFile)
			readDatabaseFile.pipe(pass)
		})
	}

	public createIndexes() {
		return this.loadDB().then(async () => {
			const levels = ['d', 'p', 'c', 'o', 'f', 'g', 's']
			this.log.info(`Creating index of every field`)
			const results: any = []
			for (const level of levels) {
				await this.db.createIndex({
					index: {
						fields: [level]
					}
				}).then((result: any) => {
					this.log.info(`Status for index for ${level}: ${result.result}`)
					results.push(result.result)
				})
			}
			return results
		})
	}

	private loadDB() {
		return new Promise((resolve, reject) => {
			if (this.db === '') {
				this.log.info(`Loading up database ${this.name}...`)
				this.db = new PouchDB(this.name)
				const dataInfo = this.db.info().then((info: any) => {
					this.log.info(`The ${info.db_name} has ${info.doc_count} records.`)
					resolve(this)
				})
			}
			else {
				this.log.info(`Database is ready.`)
				resolve()
			}
		})
	}
}

export {
	Gtdb
}