import { Gtdb } from './Gtdb'

const gtdb = new Gtdb()
const gtdbFile = './data/bac_taxonomy_r86.test.tsv'
gtdb.changeDatabaseName('gtdb-test')
    .changeGtdbFile(gtdbFile)
    .createIndexes()
    .then((msg: any) => {
        const results: boolean[] = []
        msg.forEach((item: string) => {
            results.push(item.match(/created|exists/) === null)
        })
        console.log(results)
    })