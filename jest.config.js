module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  verbose: true,
  testRegex: "(/tests/.*|(\\.|/)(tests|spec))\\.(tsx?)$"
};